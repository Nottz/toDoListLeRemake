<?php

include('../classes/Todo.php');
include('../classes/Db.php');

$idTodo = $_POST['idTodo'];
$textTodo =$_POST['textTodo'];
$doneTodo =$_POST['doneTodo'];

$tab = readData();

foreach ($tab as $object){
    if($object->getId() == $idTodo){
        $object->setText($textTodo);
        if(isset($doneTodo)){
            $object->setDone(true);
        }
        else{
            $object->setDone(false);
        }
    }
}


writeData($tab);

header('Location: ../index.php');
?>