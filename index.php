<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <title>MON SAC EST FAIT</title>
</head>
<body>

    <?php
        include("classes/Todo.php");
        include("classes/Db.php");
        $tab = readData("db/todos.txt");
        if($tab === false){
            $tab;
        }

    ?>
    <div class="container">
    <div class='to_doContainer'>
    <?php foreach($tab as $object){?>
        <div class="to_do_elements">
        <form class="todo_elements" method="post" action="forms/updateTodo.php">
                    <input type="checkbox" class="checkbox" name="doneTodo" <?php echo 'value="'.$object->getDone().'"'?><?php if($object->getDone() == true)echo 'checked'?>>
                    <input type="texte" class="text_input" name="textTodo" <?php echo'value="'.$object->getText().'"'?> required>
                    <input  type="image" src="assets/save.png" value="submit" class="save_button">
                    <input type="hidden" name="idTodo" <?php echo'value="'.$object->getId().'"'?>>
                </form>

                <form class="delete_elements" method="post" action="forms/deleteTodo.php">
                    <input  type="image" src="assets/imgDelete.png" value="submit" class="delete_button">
                    <input type="hidden" name="idTodo" <?php echo'value="'.$object->getId().'"'?>>
                </form>
                </div>
        <?php } ?>
        </div>

        <div class="texte_simple"><hr>Ajouter un toDo:</div>
                <form action="forms/addTodo.php" class="add_elements" method="post">
                    <textarea class="textarea" cols="30" rows="5" placeholder="Add something to do..." name="to_do" required></textarea>
                    <input id="submit" type="submit" value="Add">
                </form>
                <form action="forms/cleanTodo.php" method="POST">
                <input  type="image" src="assets/imgDelete.png" value="submit" class="clean_button">
                </form>
        </div>
</body>


</html>